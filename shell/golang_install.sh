#!/bin/bash

gofile=`curl https://go.dev/dl/ | grep -i linux-amd64.tar.gz  | sed -n '1p' | cut -d'=' -f3 | cut -d'/' -f3 | cut -d'"' -f1`

gofile_url="https://go.dev/dl/$gofile"


cd /tmp/
wget -c $gofile_url
rm -rf /usr/local/go && tar -C /usr/local -xzf $gofile

echo "export PATH=$PATH:/usr/local/go/bin" >> $HOME/.profile
source $HOME/.profile

