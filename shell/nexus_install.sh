#!/bin/bash

#安装依赖包
apt update
apt upgrade
apt install -y build-essential pkg-config libssl-dev git-all


useradd -s /bin/bash -m nexus
#安装nexus
su - nexus -c "curl --proto '=https' --tlsv1.2 -sSf -o rustup.sh https://sh.rustup.rs"
su - nexus -c "bash rustup.sh"
su - nexus -c "curl -o nexus.sh https://cli.nexus.xyz/"
su - nexus -c "bash nexus.sh"

