#!/usr/bin/bash

#### 定义输出颜色，github上炒回来的
_red() {
    printf '\033[0;31;31m%b\033[0m' "$1"
}

_green() {
    printf '\033[0;31;32m%b\033[0m' "$1"
}

_yellow() {
    printf '\033[0;31;33m%b\033[0m' "$1"
}

_blue() {
    printf '\033[0;31;36m%b\033[0m' "$1"
}

#[ $(id -u) != "0" ] && { echo "$(_red 你当前不是以root权限执行,请以root权限执行脚本)"; exit 1; }

function main(){
    echo "0. 显示系统信息"
    echo "1. 调用system_init"
    echo "2. 安装v2ray_ws_h3c_caddy"
    echo "3. 安装xray_vless_reality"
    echo "4. 安装docker"
    echo "5. 安装golang"
    echo "6. 按照nexus"
    echo "7. 安装sixgpt(需要先安装docker)"
    echo "8. 安装glacierlabs节点"
    echo "q. 退出脚本"
}

export tempdir="/tmp/tempinstall"
mkdir -p $tempdir
while true
do
    main
    read -p "$(_blue 你的选择: )" choice
    case $choice in
	0)
	    #dependence curl
	    if [ -f $tempdir/system_info.sh ] ;then
		source $tempdir/system_info.sh
	    else
		curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/system_info.sh  -o $tempdir/system_info.sh && source $tempdir/system_info.sh
	    fi
	    system_info_print
	    ;;
	1)
	    #dependence curl
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/system_init.sh | bash
	    ;;
	2)
	    #dependence curl
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/v2ray_ws_h2c_caddy_install_vps.sh -o $tempdir/v2ray_ws_h2c_caddy_install_vps.sh
	    read -p "$(_blue 请提供一个域名: )" domain
	    bash $tempdir/v2ray_ws_h2c_caddy_install_vps.sh $domain
	    ;;
	3)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/xray_vless_reality_install_vps.sh -o $tempdir/xray_vless_reality_install_vps.sh
	    bash $tempdir/xray_vless_reality_install_vps.sh
	    ;;
	4)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/docker_install.sh -o $tempdir/docker_install.sh
	    bash $tempdir/docker_install.sh
	    ;;
	5)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/golang_install.sh -o $tempdir/golang_install.sh
	    bash $tempdir/golang_install.sh
	    ;;
	6)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/nexus_install.sh -o $tempdir/nexus_install.sh
	    bash $tempdir/nexus_install.sh
	    ;;
	7)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/sixgpt_install.sh -o $tempdir/sixgpt_install.sh
	    bash $tempdir/sixgpt_install.sh
	    ;;
	8)
	    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/glacierlabs_install.sh -o $tempdir/glacierlabs_install.sh
	    bash $tempdir/glacierlabs_install.sh
	    ;;
	'q')
	    echo "$(_blue 已清除临时文件.)" && rm -rf $tempdir
	    exit 1
	    ;;
	*)
	    echo "$(_red 输入错误,请重新输入)"
	    ;;
    esac
done	 
