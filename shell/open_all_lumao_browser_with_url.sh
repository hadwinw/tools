#!/usr/bin/bash

bookmark_path="$HOME/.config/BraveSoftware/Brave-Browser/Default/Bookmarks"
urls=""

#从bookmark中读取需要打开的url列表,读入参数bookmark路径
function get_url(){
    urls=`jq -c -r  '.roots.bookmark_bar.children[0].children[5].children[0].children[].url' $1`
    urls="$urls"
}


#哪些浏览器会被打开
function open_browser(){
    for i in hadwin bshit chelin cindywall cupnoodles coindoever
    #for i in coindoever
    do
	if test $i = 'hadwin' ; then
	    hadwin_urls="https://launchpad.ally.build/zh-CN"
	    hadwin_urls="$urls $hadwin_urls"
	    /usr/bin/brave-browser-stable %U --password-store=basic $hadwin_urls & > /dev/null 2>&1
	else
	    /usr/bin/google-chrome-stable %U --password-store=basic --user-data-dir=/home/had/.config/$i-chrome $urls & > /dev/null 2>&1
	fi
	sleep 5
    done
}



get_url $bookmark_path
open_browser



