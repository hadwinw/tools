#!/usr/bin/bash


if [ -f $tempdir/system_info.sh ] ;then
    source $tempdir/system_info.sh
else
    curl -sL https://gitlab.com/hadwinw/tools/-/raw/main/shell/system_info.sh  -o $tempdir/system_info.sh && source $tempdir/system_info.sh
fi


#[ -z $1 ] &&  echo "$(_red "The script need a domain for caddy, eg: "$0 DOMAIN"")" && exit 1
#domain=$1

function deps_install(){
    pkg_method
    if [ $os_like = 'rhel' ];then
	$pkg_install curl yum-plugin-copr
    elif [ $os_like = 'debian' ];then
	apt update
	$pkg_install curl debian-keyring debian-archive-keyring apt-transport-https
    fi	
}

xray_install(){
    useradd -r -M -s `which nologin` xray
    bash -c "$(curl -L https://github.com/XTLS/Xray-install/raw/main/install-release.sh)" @ install
    sed -i 's@User=nobody@User=xray@g' /etc/systemd/system/xray.service
    sed -i 's@User=nobody@User=xray@g' /etc/systemd/system/xray@.service
    systemctl daemon-reload
}

xray_config(){
    cat > /usr/local/etc/xray/config.json << EOF
{
    "inbounds": [
        {
            "listen": "0.0.0.0",
            "port": 443,
            "protocol": "vless",
            "settings": {
                "clients": [
                    {
                        "id": "0fd36365-2d5d-45c9-a5dc-6bba190cd536",
                        "flow": "xtls-rprx-vision"
                    }
                ],
                "decryption": "none"
            },
            "streamSettings": {
                "network": "tcp",
                "security": "reality",
                "realitySettings": {
                    "show": false,
                    "dest": "$2:443",
                    "xver": 0,
                    "serverNames": [
                        "$2"
                    ],
                    "privateKey": "$1",
                    "minClientVer": "",
                    "maxClientVer": "",
                    "maxTimeDiff": 0,
                    "shortIds": [
                        "ffaa"
                    ]
                }
            }
        }
    ],
    "outbounds": [
        {
            "protocol": "freedom",
            "tag": "direct"
        },
        {
            "protocol": "blackhole",
            "tag": "blocked"
        }
    ]    
}

EOF
    
    service_control xray
}


deps_install
xray_install

keyfile="/root/xray_key"
if ! test -e $keyfile;then
    `which xray` x25519 > $keyfile
fi


dest="bullit.pp.ua"
private_key=`grep Private $keyfile | cut -d: -f2 | sed 's/^ //'`
    
xray_config $private_key $dest
public_key=`grep Public $keyfile | cut -d: -f2`

echo "客户端使用的域名为： $dest       公钥为： $public_key"
