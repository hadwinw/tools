#!/bin/bash

source $HOME/projects/tools/shell/env_path/different_browser_path.sh
bookmark_source_path="/home/had/.config/BraveSoftware/Brave-Browser/Default/"


#复制主浏览器的书签到其他浏览器中
for browser_path in ${different_browser_dirs[@]}
do
    if [[ $browser_path == $bookmark_source_path ]];then
        continue
    else
        echo "$browser_path"
        cp $bookmark_source_path/Bookmarks $browser_path/Bookmarks
    fi
done
