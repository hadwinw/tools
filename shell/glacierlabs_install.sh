#!/bin/bash

read -p '请输入私钥： ' private_key


sofeware_url="https://github.com/Glacier-Labs/node-bootstrap/releases/download/v0.0.2-beta/verifier_linux_amd64"
wget -c $sofeware_url
chmod u+x verifier_linux_amd64

config_url="https://glacier-labs.github.io/node-bootstrap/config.yaml"
wget -c $config_url
sed -i "s@YourPrivateKey@$private_key@g" config.yaml


screen -S mine ./verifier_linux_amd64
