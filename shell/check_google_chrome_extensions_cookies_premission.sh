#!/bin/bash

_red() {
    printf '\033[0;31;31m%b\033[0m' "$1"
}

_green() {
    printf '\033[0;31;32m%b\033[0m' "$1"
}

_yellow() {
    printf '\033[0;31;33m%b\033[0m' "$1"
}

_blue() {
    printf '\033[0;31;36m%b\033[0m' "$1"
}



#检查所有google chrome类噶浏览器的扩展是否可以读取cookies
extensions_dirs=( \
"$HOME/.config/BraveSoftware/Brave-Browser/Default/Extensions" \
"$HOME/.config/bshit-chrome/Default/Extensions" \
"$HOME/.config/chelin-chrome/Default/Extensions" \
"$HOME/.config/cindywall-chrome/Default/Extensions" \
"$HOME/.config/cupnoodles-chrome/Default/Extensions" \
"$HOME/.config/coindoever-chrome/Default/Extensions" )


#循环所有浏览器噶扩展路径
for extensions_dir in ${extensions_dirs[@]}
do
    _blue "$extensions_dir \n"

    #查找是否具有cookies权限噶扩展，输入扩展路径和是扩展名称
    for extensions_path in `find $extensions_dir -iname manifest.json`
    do
        if grep -i cookie $extensions_path; then
        _red "有权限读取cookies的扩展路径为: $extensions_path \n"
        extensions_name=`grep -i default_title $extensions_path | cut -d':' -f2`
        _red "此扩展为:$extensions_name \n"
        fi
    done
    echo -e "\n\n"
done

